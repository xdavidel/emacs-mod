;;; -*- lexical-binding: t -*-

;; Loading eraly-init.el if Emacs version < 27
(unless (featurep 'early-init)
  (load (expand-file-name "early-init" user-emacs-directory)))

;; Profile emacs startup
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "Emacs loaded in %s with %d garbage collections."
                     (emacs-init-time)
                     gcs-done)))

;; Check the system used
(defconst IS-LINUX   (eq system-type 'gnu/linux))
(defconst IS-MAC     (eq system-type 'darwin))
(defconst IS-BSD     (or IS-MAC (eq system-type 'berkeley-unix)))
(defconst IS-WINDOWS (memq system-type '(cygwin windows-nt ms-dos)))

(defvar user-emacs-directory-orig user-emacs-directory
  "user-emacs-directory before any changes")

(defvar user-emacs-tmp-dir (expand-file-name (format "emacs%d" (user-uid)) temporary-file-directory)
  "A directory to save emacs temporary files")


;; Change emacs user directory to keep the real one clean
(setq user-emacs-directory (let* ((cache-dir-xdg (getenv "XDG_CACHE_HOME"))
                                 (cache-dir (if cache-dir-xdg (concat cache-dir-xdg "/emacs")
                                              (expand-file-name "~/.cache/emacs"))))
                            (mkdir cache-dir t)
                            cache-dir))

;; Add the modules folder to the load path
(add-to-list 'load-path (expand-file-name "modules/" user-emacs-directory-orig))

;; Set default coding system (especially for Windows)
(set-default-coding-systems 'utf-8)
(customize-set-variable 'visible-bell nil)  ; turn off beeps, and no flash!
(customize-set-variable 'large-file-warning-threshold 100000000) ;; change to ~100 MB
(customize-set-variable 'ring-bell-function 'ignore) ;; Disable audio bells

;; The custom file
(customize-set-variable 'custom-file
                        (expand-file-name "custom.el" user-emacs-directory-orig))
(add-hook 'after-init-hook (lambda ()
                              (interactive)
                              (customize-save-customized)
                              (load custom-file t)))

;; Use package.el or straight.el.
;; This depends on the value of `package-enable-at-startup'
(if package-enable-at-startup
    (progn
      (defmacro install-package (package)
        `(unless (package-installed-p ,package) (package-install ,package)))

      ;;; package configuration
      (require 'package)

      ;; Emacs 27.x has gnu elpa as the default
      ;; Emacs 28.x adds the nongnu elpa to the list by default, so only
      ;; need to add nongnu when this isn't Emacs 28+
      (when (version< emacs-version "28")
        (add-to-list 'package-archives '("nongnu" . "https://elpa.nongnu.org/nongnu/")))
      (add-to-list 'package-archives '("stable" . "https://stable.melpa.org/packages/"))
      (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

      (customize-set-variable 'package-archive-priorities
                              '(("gnu"    . 99)   ; prefer GNU packages
                                ("nongnu" . 80)   ; use non-gnu packages if
                                        ; not found in GNU elpa
                                ("stable" . 70)   ; prefer "released" versions
                                        ; from melpa
                                ("melpa"  . 0)))  ; if all else fails, get it
                                        ; from melpa

      (package-initialize)
      (require 'seq)
      ;; Only refresh package contents once per day on startup, or if the
      ;; `package-archive-contents' has not been initialized. If Emacs has
      ;; been running for a while, user will need to manually run
      ;; `package-refresh-contents' before calling `package-install'.
      (when (or (seq-empty-p package-archive-contents)
                (lambda()
                  (let ((today (time-to-days (current-time)))
                        result)
                    (dolist (archive package-archives result)
                      (let* ((archive-name (expand-file-name (format "archives/%s/archive-contents" archive)
                                                             package-user-dir))
                             (archive-modified-date (time-to-days
                                                     (file-attribute-modification-time
                                                      (file-attributes archive-name)))))
                        (when (time-less-p archive-modified-date today)
                          (setq result t))))))))
      (package-refresh-contents))
  (progn
    (defmacro install-package (package)
      `(straight-use-package ,package))
    ;; Initialize straight.el
    (defvar bootstrap-version)
    (let ((bootstrap-file
           (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
          (bootstrap-version 5))
      (unless (file-exists-p bootstrap-file)
        (with-current-buffer
            (url-retrieve-synchronously
             "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
             'silent 'inhibit-cookies)
          (goto-char (point-max))
          (eval-print-last-sexp)))
      (load bootstrap-file nil 'nomessage))))


;; Load modules here
(require 'm-helpers)
(require 'm-defaults)
(require 'm-screencast)
(require 'm-ui)
(require 'm-editing)
(require 'm-evil)
(require 'm-completion)
(require 'm-windows)
(require 'm-project)
(require 'm-org)
(require 'm-lsp)
(require 'm-keybinds)
(require 'm-tools)

;; Make GC pauses faster by decreasing the threshold.
(setq gc-cons-threshold (* 2 1000 1000))
