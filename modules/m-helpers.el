;;; m-helpers.el -*- lexical-binding: t; -*-

(defun open-config-dir ()
  "Open configuration directory"
  (interactive)
    (find-file user-emacs-directory-orig))

(defun clear-bg ()
  "Clearing the background of the current frame"
  (interactive)
  (set-face-background 'default "unspecified-bg" (selected-frame)))

(defvar after-load-theme-hook nil
  "hook run after a color theme is loaded using `load-theme'.")
(defadvice load-theme (after run-after-load-theme activate)
  "run `after-load-theme-hook'."
  (run-hooks 'after-load-theme-hook))

(provide 'm-helpers)
;;; m-helpers.el ends here
