;;; m-evil.el -*- lexical-binding: t; -*-

;; Evil mode configuration, for a `vi' keybindings.

;; Install dependencies
(install-package 'evil)
(install-package 'undo-tree)
(install-package 'evil-surround)
(install-package 'evil-collection)
(install-package 'evil-nerd-commenter)
(unless (display-graphic-p)
  (install-package 'evil-terminal-cursor-changer))


;; Turn on undo-tree globally
(setq undo-tree-history-directory-alist (list (cons "." (concat user-emacs-directory "undo-tree-hist/"))))
(global-undo-tree-mode)


;; Set some variables that must be configured before loading the package
(customize-set-variable 'evil-want-integration t)
(customize-set-variable 'evil-want-keybinding nil)
(customize-set-variable 'evil-want-C-u-scroll t)
(customize-set-variable 'evil-want-C-i-jump nil)
(customize-set-variable 'evil-respect-visual-line-mode t)
(customize-set-variable 'evil-undo-system 'undo-tree)
(customize-set-variable 'evil-want-Y-yank-to-eol t)
(customize-set-variable 'evil-vsplit-window-right t)
(customize-set-variable 'evil-split-window-below t)

;; Load Evil and enable it globally
(require 'evil)
(evil-mode 1)

;; Make C-g revert to normal state
(define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)

;; Rebind `universal-argument' to 'C-M-u' since 'C-u' now scrolls the buffer
(global-set-key (kbd "C-M-u") 'universal-argument)

;; C-h is backspace in insert state
(define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)

;; Use visual line motions even outside of visual-line-mode buffers
(evil-global-set-key 'motion "j" 'evil-next-visual-line)
(evil-global-set-key 'motion "k" 'evil-previous-visual-line)

;; Set a global binding for better line commenting/uncommenting
(global-set-key (kbd "C-/") 'evilnc-comment-or-uncomment-lines)
(define-key global-map [remap comment-dwim] #'evilnc-comment-or-uncomment-lines)
(evil-global-set-key 'normal "gcc" 'evilnc-comment-or-uncomment-lines)
(evil-global-set-key 'visual "gc"  'evilnc-comment-or-uncomment-lines)

;; Allow arrows as replacement for hjkl
(define-key evil-window-map (kbd "<left>") 'evil-window-left)
(define-key evil-window-map (kbd "<up>") 'evil-window-up)
(define-key evil-window-map (kbd "<right>") 'evil-window-right)
(define-key evil-window-map (kbd "<down>") 'evil-window-down)

;; Vim like surround package
(global-evil-surround-mode)

;; Make sure some modes start in Emacs state
;; TODO: Split this out to other configuration modules?
(dolist (mode '(custom-mode
                eshell-mode
                term-mode))
  (add-to-list 'evil-emacs-state-modes mode))

(evil-collection-init)

;; Terminal cursor mode support
(unless (display-graphic-p)
  (evil-terminal-cursor-changer-activate))

(provide 'm-evil)
;;; m-evil.el ends here
