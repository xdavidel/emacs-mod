;;; m-editing.el -*- lexical-binding: t; -*-

;; Editing text configuration.

(install-package 'goggles)

;; parentheses
(electric-pair-mode 1) ; auto-insert matching bracket
(show-paren-mode 1)    ; turn on paren match highlighting

;; whitespace
(customize-set-variable 'whitespace-style
                        '(face tabs empty trailing tab-mark indentation::space))
(customize-set-variable 'whitespace-action '(cleanup auto-cleanup))
(add-hook 'prog-mode-hook #'whitespace-mode)
(add-hook 'text-mode-hook #'whitespace-mode)

;; Highlight modified region
(customize-set-variable 'goggles-pulse t)
(add-hook 'text-mode-hook 'goggles-mode)
(add-hook 'prog-mode-hook 'goggles-mode)

(provide 'm-editing)
;;; m-editing.el ends here
