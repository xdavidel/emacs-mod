;;; m-defaults.el -*- lexical-binding: t; -*-

(unless (featurep 'so-long)
  (install-package 'so-long))

;; General sane defaults

;; Revert Dired and other buffers
(customize-set-variable 'global-auto-revert-non-file-buffers t)

;; Revert buffers when the underlying file has changed
(global-auto-revert-mode 1)

;; No line wrap be default
(setq-default truncate-lines t)

;; Use spaces instead of tabs
(setq-default indent-tabs-mode nil)

;; Tabs is this much spaces
(setq-default tab-width 2)

;; No Lock Files
(customize-set-variable 'create-lockfiles nil)

;; Increase autosave times
(setq auto-save-interval 2400
      auto-save-timeout 300)

;; create directory for auto-save-mode
(let* ((auto-save-path (concat user-emacs-directory "/auto-saves/"))
       (auto-save-sessions-path (concat auto-save-path "sessions/")))
  (make-directory auto-save-path t)
  (setq auto-save-list-file-prefix auto-save-sessions-path
        auto-save-file-name-transforms `((".*" ,auto-save-path t))))

;; Put backups elsewhere
(setq backup-directory-alist
      `(("." . ,(concat user-emacs-directory "backup")))
      backup-by-copying t     ; Use copies
      version-control t       ; Use version numbers on backups
      delete-old-versions t   ; Automatically delete excess backups
      kept-new-versions 10    ; Newest versions to keep
      kept-old-versions 5     ; Old versions to keep
      backup-enable-predicate
      (lambda (name)
        (and (normal-backup-enable-predicate name)
             (not
              (let ((method (file-remote-p name 'method)))
                (when (stringp method)
                  (member method '("su" "sudo" "doas"))))))))

;; N.B. Emacs 28 has a variable for using short answers, which should
;; be preferred if using that version or higher.
(if (boundp 'use-short-answers)
    (setq use-short-answers t)
  (advice-add 'yes-or-no-p :override #'y-or-n-p))

;; Turn on recentf mode
(add-hook 'after-init-hook #'recentf-mode)
(customize-set-variable 'recentf-save-file
                        (expand-file-name "recentf" user-emacs-directory))

;; Do not saves duplicates in kill-ring
(customize-set-variable 'kill-do-not-save-duplicates t)

;; Make scrolling less stuttered
(setq auto-window-vscroll nil)
(customize-set-variable 'fast-but-imprecise-scrolling t)
(customize-set-variable 'scroll-conservatively 101)
(customize-set-variable 'scroll-margin 0)
(customize-set-variable 'scroll-preserve-screen-position t)

;; Middle-click paste at point, not at cursor.
(setq mouse-yank-at-point t)

;; Better support for files with long lines
(setq-default bidi-paragraph-direction 'left-to-right)
(setq-default bidi-inhibit-bpa t)
(global-so-long-mode 1)

;; Make shebang (#!) file executable when saved
(add-hook 'after-save-hook #'executable-make-buffer-file-executable-if-script-p)

;; Enable savehist-mode for an command history
(savehist-mode 1)
(customize-set-variable 'savehist-file
                        (expand-file-name "history" user-emacs-directory))

;; User profile
(setq user-full-name "David Delarosa"
      user-mail-address "xdavidel@gmail.com")

(provide 'm-defaults)
;;; m-defaults.el ends here
