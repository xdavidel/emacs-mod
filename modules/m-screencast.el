;;; m-screencast.el -*- lexical-binding: t; -*-

;; Screencast configuration

(install-package 'keycast)

(customize-set-variable 'keycast-remove-tail-elements nil)
(customize-set-variable 'keycast-insert-after 'mode-line-misc-info)

(provide 'm-screencast)
;;; m-screencast.el ends here
