;;; m-keybinds.el -*- lexical-binding: t; -*-

(global-set-key (kbd "C-c C-.") #'open-config-dir)

(provide 'm-keybinds)
;;; m-keybinds.el ends here
