;;; m-org.el  -*- lexical-binding: t; -*-

(install-package 'org)
(install-package 'org-appear)

;; Return or left-click with mouse follows link
(customize-set-variable 'org-return-follows-link t)
(customize-set-variable 'org-mouse-1-follows-link t)

;; Don't ask before code evaluation
(customize-set-variable 'org-confirm-babel-evaluate nil)

;; Display links as the description provided
(customize-set-variable 'org-descriptive-links t)

;; Don't show inline images at startup
(customize-set-variable 'org-startup-with-inline-images nil)

;; Indentation size for org source blocks
(customize-set-variable 'org-edit-src-content-indentation 2)

;; Important for indentation sensitive languages
(customize-set-variable 'org-src-preserve-indentation t)

;; One start is fine
(customize-set-variable 'org-hide-leading-stars t)

;; Delay until org document is being opend
(add-hook 'org-mode-hook (lambda()
                           ;; change header size on different levels
                           (dolist (face '((org-level-1 . 1.2)
                                           (org-level-2 . 1.1)
                                           (org-level-3 . 1.05)
                                           (org-level-4 . 1.0)
                                           (org-level-5 . 1.1)
                                           (org-level-6 . 1.1)
                                           (org-level-7 . 1.1)
                                           (org-level-8 . 1.1)))
                             (set-face-attribute (car face) nil :height (cdr face)))

                           ;; Add elisp and [c, cpp, D] languages
                           (org-babel-do-load-languages
                            'org-babel-load-languages
                            '((emacs-lisp . t)
                              (C          . t)
                              (shell      . t)
                              (js         . t)
                              (clojure    . t)
                              (python     . t)
                              ))


                           ;; Hide markup markers
                           (customize-set-variable 'org-hide-emphasis-markers t)
                           (org-appear-mode)

                           ;; Org programming languages templates
                           (when
                               (not
                                (version<= org-version "9.1.9"))
                             (require 'org-tempo)
                             (add-to-list 'org-structure-template-alist
                                          '("sh" . "src shell"))
                             (add-to-list 'org-structure-template-alist
                                          '("el" . "src emacs-lisp"))
                             (add-to-list 'org-structure-template-alist
                                          '("py" . "src python"))
                             (add-to-list 'org-structure-template-alist
                                          '("cpp" . "src cpp"))
                             (add-to-list 'org-structure-template-alist
                                          '("lua" . "src lua"))
                             (add-to-list 'org-structure-template-alist
                                          '("go" . "src go")))
                            (setq-local electric-pair-inhibit-predicate
                                `(lambda (c)
                                    (if (char-equal c ?<) t (,electric-pair-inhibit-predicate c))))
                           ))

(provide 'm-org)
;;; m-org.el ends here
