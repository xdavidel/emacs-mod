;;; m-lsp.el -*- lexical-binding: t; -*-

;; Commentary

;; Setup lsp packages.

;;; Code:

(install-package 'eglot)


(provide 'm-lsp)
;;; m-lsp.el ends here
