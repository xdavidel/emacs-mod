;;; m-windows.el -*- lexical-binding: t; -*-

;; Emacs windows configuration.

(defcustom windows-prefix-key "C-c w"
  "Configure the prefix key for windows bindings.")

(winner-mode 1)

(define-prefix-command 'windows-key-map)

(define-key 'windows-key-map (kbd "u") 'winner-undo)
(define-key 'windows-key-map (kbd "n") 'windmove-down)
(define-key 'windows-key-map (kbd "p") 'windmove-up)
(define-key 'windows-key-map (kbd "b") 'windmove-left)
(define-key 'windows-key-map (kbd "f") 'windmove-right)

(global-set-key (kbd windows-prefix-key) 'windows-key-map)

(provide 'm-windows)
;;; m-windows.el ends here
