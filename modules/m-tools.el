;;; m-tools.el -*- lexical-binding: t; -*-

;; More tools to include in emacs
(install-package 'magit)
(install-package 'ranger)

;; Ranger
(global-set-key (kbd "M-e") 'ranger)
(customize-set-variable 'ranger-width-preview 0.5)
(setq ranger-show-literal nil
    ranger-override-dired-mode t)

;; Magit
(global-set-key (kbd "C-x g") 'magit-status)

(provide 'm-tools)
