;;;; m-project.el --- Starting configuration for project management  -*- lexical-binding: t; -*-

;; Provides default settings for project management with project.el

(customize-set-variable 'project-list-file (expand-file-name "projects" user-emacs-directory))

(provide 'm-project)
;;; m-project.el ends here
