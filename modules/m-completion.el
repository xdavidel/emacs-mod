;;; m-completion.el -*- lexical-binding: t; -*-

;; Setup completion packages. Completion in this sense is more like
;; narrowing, allowing the user to find matches based on minimal
;; inputs and "complete" the commands, variables, etc from the
;; narrowed list of possible choices.

(install-package 'vertico)
(install-package 'consult)
(install-package 'orderless)
(install-package 'marginalia)
(install-package 'embark)
(install-package 'corfu)
(install-package '(corfu-doc :type git :host github :repo "galeo/corfu-doc"))
(install-package '(svg-lib :type git :host github :repo "rougier/svg-lib"))
(install-package '(kind-icon :type git :host github :repo "jdtsmith/kind-icon"))
(install-package 'cape)

;;;; Vertico
(require 'vertico)
(require 'vertico-directory "extensions/vertico-directory.el")

(with-eval-after-load 'evil
  (define-key vertico-map (kbd "C-j") 'vertico-next)
  (define-key vertico-map (kbd "C-k") 'vertico-previous)
  (define-key vertico-map (kbd "M-h") 'vertico-directory-up))

;; Cycle back to top/bottom result when the edge is reached
(customize-set-variable 'vertico-cycle t)

;; Vertico background color
(custom-set-faces (backquote (vertico-current ((t (:background "#3a3f5a"))))))

;; Start Vertico
(vertico-mode 1)

;;;; Marginalia

;; Configure Marginalia
(require 'marginalia)
(customize-set-variable 'marginalia-annotators '(marginalia-annotators-heavy marginalia-annotators-light nil))
(marginalia-mode 1)

;;;; Consult

;; Simple preview - no hooks
(customize-set-variable 'consult-preview-raw-size 1)

;; Set some consult bindings
(global-set-key (kbd "C-x b") 'consult-buffer)  ; enhanced switch to buffer
(global-set-key (kbd "M-s")   'consult-outline) ; navigation by headings
(global-set-key (kbd "C-s")   'consult-line)    ; search lines with preview
(global-set-key (kbd "C-c r") 'consult-ripgrep) ; search lines with preview
(global-set-key (kbd "C-c f") 'consult-find)    ; search lines with preview

;; (consult-customize
;;  consult-buffer consult-bookmark :preview-key '(:debounce 0.5 any)
;;  consult-theme :preview-key '(:debounce 1 any)
;;  consult-line :preview-key '(:debounce 0 any))
;; (dolist (hook
;;          '(change-major-mode-after-body-hook))
;;   (push hook #consult-preview-excluded-hooks))

;; use 'fd' instead of 'find' if exists in system
(when (executable-find "fd")
  (setq consult-find-args "fd --hidden"))

(define-key minibuffer-local-map (kbd "C-r") 'consult-history)

(setq completion-in-region-function #'consult-completion-in-region)

;;;; Orderless

;; Set up Orderless for better fuzzy matching
(require 'orderless)
(customize-set-variable 'completion-styles '(orderless))
(customize-set-variable 'completion-category-overrides '((file (styles . (partial-completion)))))
(setq completion-category-defaults nil
      read-file-name-completion-ignore-case t)

;;;; Corfu

;; Enable cycling for `corfu-next/previous'
(customize-set-variable 'corfu-cycle t)

;; Enable auto completion
(customize-set-variable 'corfu-auto t)

;; Do not commit selected candidates on next input
(customize-set-variable 'corfu-commit-predicate nil)

;; Automatically quit at word boundary
(customize-set-variable 'corfu-quit-at-boundary t)

;; Automatically quit if there is no match
(customize-set-variable 'corfu-quit-no-match t)

;; Disable current candidate preview
(customize-set-variable 'corfu-preview-current nil)

;; Disable candidate preselection
(customize-set-variable 'corfu-preselect-first nil)

;; Show documentation in the echo area
(customize-set-variable 'corfu-echo-documentation nil)

;; Use scroll margin
(customize-set-variable 'corfu-scroll-margin 5)

(setq corfu-auto-delay 0.4
      corfu-auto-prefix 1)

(global-set-key (kbd "C-SPC") 'completion-at-point)

(corfu-global-mode)
(define-key corfu-map (kbd "<tab>") 'corfu-complete)
(define-key corfu-map (kbd "C-n") 'corfu-next)
(define-key corfu-map (kbd "C-p") 'corfu-previous)
(define-key corfu-map (kbd "<escape>") 'corfu-quit)
(with-eval-after-load 'eshell
  (add-hook eshell-mode-hook (lambda() (set-local corfu-auto nil))))


;;;; Kind Icons
(customize-set-variable 'kind-icon-default-face 'corfu-default)
(with-eval-after-load 'corfu
  (require 'kind-icon)
(add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))


;; Corfu-doc
(customize-set-variable 'corfu-doc-delay 0.5)
(customize-set-variable 'corfu-doc-max-width 70)
(customize-set-variable 'corfu-doc-max-height 20)
(add-hook 'corfu-mode-hook #'corfu-doc-mode)

;;;; Cape
;; `completion at point' extensions for specific candidates in `completion in region'
(setq dabbrev-upcase-means-case-search t)

;; Add `completion-at-point-functions', used by `completion-at-point'.
(add-to-list 'completion-at-point-functions #'cape-file)     ;; Complete file name
(add-to-list 'completion-at-point-functions #'cape-dabbrev)  ;; Complete word from current buffers
(add-to-list 'completion-at-point-functions #'cape-keyword)  ;; Complete programming language keyword
(add-to-list 'completion-at-point-functions #'cape-tex)      ;; Complete unicode char from TeX command
(add-to-list 'completion-at-point-functions #'cape-sgml)     ;; Complete unicode char from Sgml entity
(add-to-list 'completion-at-point-functions #'cape-rfc1345)  ;; Complete unicode char using RFC 1345 mnemonics


;;;; Embark

(global-set-key [remap describe-bindings] #'embark-bindings)
(global-set-key (kbd "C-.") 'embark-act)

;; Use Embark to show bindings in a key prefix with `C-h`
(setq prefix-help-command #'embark-prefix-help-command)

(provide 'm-completion)
;;; m-completion.el ends here
