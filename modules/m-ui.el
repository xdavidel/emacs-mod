;;; m-ui.el -*- lexical-binding: t; -*-

;; User interface customizations. Examples are the modeline and how
;; help buffers are displayed.

(install-package 'all-the-icons)
(install-package 'doom-themes)
(install-package 'modus-themes)
(install-package 'elisp-demos)
(install-package 'helpful)

;;;; Font

;; Change for size
(global-set-key (kbd "C-=") #'text-scale-increase)
(global-set-key (kbd "C--") #'text-scale-decrease)
(global-set-key (kbd "<C-mouse-4>") #'text-scale-increase)
(global-set-key (kbd "<C-mouse-5>") #'text-scale-decrease)

(defun font-installed-p (font-name)
  "Check if font with FONT-NAME is available."
  (find-font (font-spec :name font-name)))

;; Set fonts if possible
(cond ((font-installed-p "Cascadia Code")
       (set-face-attribute 'default nil :font "Cascadia Code 10"))
      ((font-installed-p "JetBrainsMono")
       (set-face-attribute 'default nil :font "JetBrainsMono 10"))
      ((font-installed-p "Hack")
       (set-face-attribute 'default nil :font "Hack 10")))

;;;; Help Buffers

;; Make `describe-*' screens more helpful
(require 'helpful)
(define-key helpful-mode-map [remap revert-buffer] #'helpful-update)
(global-set-key [remap describe-command] #'helpful-command)
(global-set-key [remap describe-function] #'helpful-callable)
(global-set-key [remap describe-key] #'helpful-key)
(global-set-key [remap describe-symbol] #'helpful-symbol)
(global-set-key [remap describe-variable] #'helpful-variable)
(global-set-key (kbd "C-h F") #'helpful-function)

;; Bind extra `describe-*' commands
(global-set-key (kbd "C-h K") #'describe-keymap)

;;;; Line Numbers
(customize-set-variable 'display-line-numbers-width 4)
(customize-set-variable 'display-line-numbers-width-start t)
(customize-set-variable 'display-line-numbers-type 'relative)
(unless (fboundp 'display-line-numbers-mode)
  (autoload #'display-line-numbers-mode "display-line-numbers" nil t))
(add-hook 'prog-mode-hook #'display-line-numbers-mode)

;;;; Elisp-Demos

;; also add some examples
(require 'elisp-demos)
(advice-add 'helpful-update :after #'elisp-demos-advice-helpful-update)

;; ;; add visual pulse when changing focus, like beacon but built-in
;; ;; from from https://karthinks.com/software/batteries-included-with-emacs/
(defun pulse-line (&rest _)
      "Pulse the current line."
      (pulse-momentary-highlight-one-line (point)))
(dolist (cmd '(recenter-top-bottom
                   other-window windmove-do-window-select
                   pager-page-down pager-page-up
                   winum-select-window-by-number
                   ;; treemacs-select-window
                   symbol-overlay-basic-jump))
      (advice-add cmd :after #'pulse-line))

;;;; Theme
(defcustom use-light-theme nil
  "Should emacs use light theme by default")

;; Clear background on changing theme
(unless (display-graphic-p)
  (add-hook 'after-load-theme-hook #'clear-bg))

;; Add all your customizations prior to loading the themes
(setq modus-themes-italic-constructs t
      modus-themes-bold-constructs nil
      modus-themes-region '(bg-only no-extend))

;; Load the theme of your choice:
(if use-light-theme
  (load-theme 'modus-operandi t)
    (load-theme 'modus-vivendi t))

(define-key global-map (kbd "C-c t t") #'modus-themes-toggle)

(provide 'm-ui)
;;; m-ui.el ends here
